include_directories(
)

set(mbbrl_SRC
)

add_library(mbbrl SHARED ${mbbrl_SRC})

target_link_libraries(mbbrl gp)

install(TARGETS mbbrl 
	LIBRARY DESTINATION /usr/local/lib
)

file(GLOB_RECURSE headers "*.h" "*.hxx")

install(FILES ${headers} DESTINATION /usr/local/include/brll)
