# - Try to find gp
# Once done this will define
#  NPP_INCLUDE_DIR - The libn++ include directories
#  NPP_LIBRARY - The libn++ .so

FIND_PATH(NPP_INCLUDE_DIR n++/n++.h
  /usr/local/include/
)

FIND_LIBRARY( NPP_LIBRARY gp
  /usr/local/lib
)

if (NOT NPP_INCLUDE_DIR)
  MESSAGE(FATAL_ERROR "Unable to find libn++ include header files")
endif ()

if (NOT NPP_LIBRARY)
  MESSAGE(FATAL_ERROR "Unable to find libn++.a")
endif ()
