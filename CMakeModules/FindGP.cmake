# - Try to find gp
# Once done this will define
#  GP_INCLUDE_DIR - The libmbbrl include directories
#  GP_LIBRARY - The libmbbrl .so

FIND_PATH(GP_INCLUDE_DIR gp.h
  /usr/local/include
)

FIND_LIBRARY( GP_LIBRARY gp
  /usr/local/lib
)

if (NOT GP_INCLUDE_DIR)
  MESSAGE(FATAL_ERROR "Unable to find libgp include header files")
endif ()

if (NOT GP_LIBRARY)
  MESSAGE(FATAL_ERROR "Unable to find libgp.a")
endif ()
