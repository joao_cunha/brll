#ifndef GPTRANSITIONMODEL_H_
#define GPTRANSITIONMODEL_H_

#include "TransitionModel.h"
#include "GPBatchTrainer.h"
#include <gp.h>

using namespace libgp;

namespace brll
{

class GPTransitionModel : public TransitionModel
{
public:
	GPTransitionModel(GaussianProcess** gps, unsigned int stateDim, const GPBatchTrainer& trainer = GPBatchTrainer());
	virtual ~GPTransitionModel();
	virtual State mean(const State& s, const Action& a) const;
	virtual State sigma(const State& s, const Action& a) const;
	virtual State sample(const State& s, const Action& a) const;
	virtual void train(const TransitionPattern& pattern);

private:
	vector<GaussianProcess*> gps;
	GPBatchTrainer trainer;

};

}

#endif /* GPTRANSITIONMODEL_H_ */
