#include "GPBatchTrainer.h"

#include "Clock.h"

#include <Eigen/Dense>
#include <set>
#include <fstream>
#include <rprop.h>

using namespace std;

namespace brll
{

GPBatchTrainer::GPBatchTrainer(const bool & optimize, const bool & clearSet, const double & thres)
	: optimize(optimize), clearSet(clearSet), thres(thres)
{
}

GPBatchTrainer::~GPBatchTrainer()
{
}

void GPBatchTrainer::train(GaussianProcess* gp, const Eigen::MatrixXd & inputs, const Eigen::VectorXd & targets)
{
	if(clearSet) //need to clear if we are going to subsample
		gp->clear_sampleset(); //clear samples

	unsigned int inputDim = inputs.cols();
	double input[inputDim];


	unsigned int prevSize = gp->get_sampleset_size();
	for (unsigned int i = 0; i < prevSize; ++i)
	{
		double output = targets(i);

		gp->set_y(i, output); // update existing patterns
	}

	for (int i = prevSize; i < inputs.rows(); ++i)
	{
		for (unsigned int j = 0; j < inputDim; ++j)
		{
			input[j] = inputs(i, j);
		}
		gp->add_pattern(input, targets(i)); //add new patterns
	}

	double me = 0;
	for (unsigned int i = 0; i < gp->get_sampleset_size(); ++i)
	{
		for (unsigned int j = 0; j < inputDim; ++j)
		{
			input[j] = inputs(i, j);
		}
		double y = gp->f(input);
		me+= fabs(y-targets(i));
	}

	me /= gp->get_sampleset_size();

	cout << "me = " << me << ", thres = " << thres << endl;

	if(optimize && (me > thres))
	{
		Eigen::VectorXd p = gp->cf->get_loghyper();
		for (unsigned int i = 0; i < gp->cf->get_param_dim(); ++i)
			p(i) = 0.0;
		gp->cf->set_loghyper(p);
		RProp optimizer;
		gp->optimize(&optimizer, 30, true);
	}

}

}  // namespace brll
