#include "GPTransitionModel.h"

#include <gp_utils.h>

#include <vector>
#include <iostream>

using namespace std;

namespace brll
{

GPTransitionModel::GPTransitionModel(GaussianProcess** gps, unsigned int stateDim, const GPBatchTrainer& trainer)
	: gps(gps, gps + stateDim), trainer(trainer)
{
}

GPTransitionModel::~GPTransitionModel()
{
	for (unsigned int i = 0; i < gps.size(); ++i)
	{
		delete gps[i];
	}

}

State GPTransitionModel::mean(const State & s, const Action & a) const
{
	double input[s.getDim() + a.getDim()];
	double output[s.getDim()];

	for (unsigned int i = 0; i < s.getDim(); ++i)
	{
		input[i] = s.getValue(i);
	}

	for (unsigned int i = 0; i < a.getDim(); ++i)
	{
		input[i + s.getDim()] = a.getCommand(i);
	}

	for (unsigned int i = 0; i < s.getDim(); ++i)
	{
		output[i] = gps[i]->f(input);
	}

	double state[s.getDim()];

	//output is only a delta, add the state
	for (unsigned int i = 0; i < s.getDim(); ++i)
	{
		state[i] += s.getValue(i) + output[i];
	}

	State nextState = State(state, s.getDim());

	return nextState;
}

State GPTransitionModel::sigma(const State & s, const Action & a) const
{
	double input[s.getDim() + a.getDim()];
	double output[s.getDim()];

	for (unsigned int i = 0; i < s.getDim(); ++i)
	{
		input[i] = s.getValue(i);
	}

	for (unsigned int i = 0; i < a.getDim(); ++i)
	{
		input[i + s.getDim()] = a.getCommand(i);
	}

	for (unsigned int i = 0; i < s.getDim(); ++i)
	{
		output[i] = gps[i]->var(input);
	}

	double sigmas[s.getDim()];

	//output is variance, get std. dev.
	for (unsigned int i = 0; i < s.getDim(); ++i)
	{
		sigmas[i] = sqrt(output[i]);
	}

	State sigma = State(sigmas, s.getDim());

	return sigma;

}


State GPTransitionModel::sample(const State & s, const Action & a) const
{
	State mean = this->mean(s,a);
	State sigma = this->sigma(s,a);

	double samples[s.getDim()];

	for (unsigned int i = 0; i < s.getDim(); ++i)
	{
		samples[i] = Utils::randn()*sigma.getValue(i) + mean.getValue(i);
	}

	State sample = State(samples, s.getDim());

	return sample;

}

void GPTransitionModel::train(const TransitionPattern & pattern)
{
	cout << "Training Model" << endl;

	unsigned int stateDim = pattern.getTransition(0).getState().getDim();
	unsigned int actionDim = pattern.getTransition(0).getAction().getDim();

	Eigen::MatrixXd inputs(pattern.size(), stateDim + actionDim);
	vector<Eigen::VectorXd> targets;

	for (unsigned int i = 0; i < stateDim; ++i)
	{
		targets.push_back(Eigen::VectorXd(pattern.size()));
	}

	for (unsigned int i = 0; i < pattern.size(); ++i)
	{
		for (unsigned int j = 0; j < stateDim; ++j)
		{
			inputs(i, j) = pattern.getTransition(i).getState().getValue(j);
			targets[j](i) = pattern.getTransition(i).getDelta().getValue(j);
		}

		for (unsigned int j = 0; j < actionDim; ++j)
		{
			inputs(i, stateDim + j) = pattern.getTransition(i).getAction().getCommand(j);
		}
	}

	for (unsigned int i = 0; i < stateDim; ++i)
	{
		trainer.train(gps[i], inputs, targets[i]);
	}
}

}
