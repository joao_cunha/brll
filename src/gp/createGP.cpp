#include <iostream>

#include <gp.h>
#include <gp_full.h>
#include <gp_local.h>
#include <cov_factory.h>
#include <vector>
#include <string>

#include "Clock.h"

using namespace std;
using namespace libgp;
using namespace brll;


int main(int argc, char **argv)
{
	if(argc < 4 || argc > 5)
	{
		cout << "Usage: " << argv[0] << " <gpfilename> <inputDim> <covFunctions> [<random_init-0/1>]"<< endl;
		CovFactory factory;
		vector<string> names = factory.list();
		cout << "Possible covariance functions:" << endl;
		for (unsigned int i = 0; i < names.size(); ++i)
		{
			cout << names[i] << endl;
		}
		exit(1);
	}

	unsigned int inputDim = atoi(argv[2]);
	string covFunction;

	covFunction = argv[3];

	GaussianProcess* gp;

	gp = new FullGaussianProcess(inputDim, covFunction);


	unsigned int paramDim = gp->covf().get_param_dim();
	double params[paramDim];

	for (unsigned int i = 0; i < paramDim; ++i)
	{
		cout << "Hyper Parameter[" << i << "]: ";
		cin >> params[i];
		params[i] = log(params[i]);
	}

	gp->covf().set_loghyper(params);

	unsigned int random_init = 0;

	if(argc == 5)
		random_init = atoi(argv[4]);

	if(random_init == 1)
	{
		double minX = -10, maxX = 10;
		double minY = 0, maxY = 100;

		unsigned int n = 1000;

		srand48(Clock::now());

		for (unsigned int i = 0; i < n; ++i)
		{
			double x[inputDim];
			for (unsigned int j = 0; j < inputDim; ++j)
			{
				double tmp = drand48();
				cout << tmp << " -> " << tmp*(maxX-minX) << " -> " << tmp*(maxX-minX) + minX << endl;
				x[j] = tmp*(maxX-minX) + minX;
			}

			double y = drand48()*(maxY-minY) + minY;
			gp->add_pattern(x,y);
		}
	}


	gp->write(argv[1]);

	cout << "Gaussian Process written to " << argv[1] << endl;
}
