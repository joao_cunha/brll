#ifndef GPBATCHTRAINER_H_
#define GPBATCHTRAINER_H_

#include "QPattern.h"

#include <gp.h>

using namespace libgp;

namespace brll
{

class GPBatchTrainer
{
public:
	GPBatchTrainer(const bool & optimize = false, const bool & clearSet = false, const double & thres = 0);
	virtual ~GPBatchTrainer();
	virtual void train(GaussianProcess* gp, const Eigen::MatrixXd & inputs, const Eigen::VectorXd & targets);

private:
	const bool optimize;
	const bool clearSet;
	const double thres;
};

}

#endif /* GPBATCHTRAINER_H_ */
