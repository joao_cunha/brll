#include "GPQFunction.h"

namespace brll
{

GPQFunction::GPQFunction(GaussianProcess* gp, const GPBatchTrainer& trainer)
	: gp(gp), trainer(trainer)
{
}

GPQFunction::~GPQFunction()
{
	delete gp;
}

double GPQFunction::getUtility(const State& s, const Action& a) const
{
	unsigned int stateDim = s.getDim();
	unsigned int actionDim = a.getDim();
	double input[stateDim + actionDim];

	for (unsigned int i = 0; i < stateDim; ++i)
	{
		input[i] = s.getValue(i);
	}

	for (unsigned int i = 0; i < actionDim; ++i)
	{
		input[i + stateDim] = a.getCommand(i);
	}

	return gp->f(input);
}

void GPQFunction::train(const QPattern& pattern)
{
	unsigned int stateDim = pattern.getInputAt(0).getState().getDim();
	unsigned int actionDim = pattern.getInputAt(0).getAction().getDim();

	Eigen::MatrixXd inputs(pattern.size(), stateDim + actionDim);
	Eigen::VectorXd targets(pattern.size());

	for (unsigned int i = 0; i < pattern.size(); ++i)
	{
		for (unsigned int j = 0; j < stateDim; ++j)
		{
			inputs(i, j) = pattern.getInputAt(i).getState().getValue(j);
		}

		for (unsigned int j = 0; j < actionDim; ++j)
		{
			inputs(i, stateDim + j) = pattern.getInputAt(i).getAction().getCommand(j);
		}

		targets(i) = pattern.getOutputAt(i);
	}

	trainer.train(gp, inputs, targets);
}

} //namespace brll
