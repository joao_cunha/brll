#ifndef GPQFUNCTION_H_
#define GPQFUNCTION_H_

#include "QFunction.h"
#include "GPBatchTrainer.h"
#include <gp.h>

using namespace libgp;

namespace brll
{

class GPQFunction: public QFunction
{
public:
	GPQFunction(GaussianProcess* gp, const GPBatchTrainer& trainer = GPBatchTrainer());
	virtual ~GPQFunction();
	virtual double getUtility(const State& s, const Action& a) const;
	virtual void train(const QPattern& pattern);

private:
	GaussianProcess* gp;
	GPBatchTrainer trainer;
};

} //namespace brll

#endif /* GPQFUNCTION_H_ */
