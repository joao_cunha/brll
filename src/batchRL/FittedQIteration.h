#ifndef FITTEDQITERATION_H_
#define FITTEDQITERATION_H_

#include "TrainingData.h"
#include "QPattern.h"
#include "QPatternGenerator.h"
#include "QFunction.h"
#include "PreProcess.h"

#include <vector>

namespace brll
{

class FittedQIteration
{
public:
	FittedQIteration(QFunction* qFunction, const QPatternGenerator* generator,
		unsigned int iterations, bool test, QPattern hintToGoal = QPattern());
	virtual ~FittedQIteration();
	virtual void fit(const TrainingData& data);
	void addPreProcess(PreProcess* p);

protected:
	QFunction* qFunction;
	const QPatternGenerator* patternGenerator;
	std::vector<PreProcess*> preProcess;
	unsigned int iterations;
	bool test;
	QPattern hintToGoal;
};

}

#endif /* FITTEDQITERATION_H_ */
