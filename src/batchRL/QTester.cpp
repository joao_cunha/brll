#include "QTester.h"

#include <math.h>

namespace brll
{

QTester::QTester(const QFunction* qFunction, const QPattern& pattern)
	: qFunction(qFunction), pattern(pattern)
{
}

QTester::~QTester()
{
}

double QTester::meanError()
{
	double error = 0;

	for (unsigned int i = 0; i < pattern.size(); ++i)
	{
		const State& s = pattern.getInputAt(i).getState();
		const Action& a = pattern.getInputAt(i).getAction();

		outputs.push_back(qFunction->getUtility(s,a));
		error += fabs((pattern.getOutputAt(i) - outputs[i]));
	}

	return error / pattern.size();
}

void QTester::write(ostream & os)
{
	double me = meanError();

	cout << "Testing - mean error = " << me << endl;

	for (unsigned int i = 0; i < pattern.size(); ++i)
	{
		pattern.write(os, i);

		os << " -> " << outputs[i] << endl;
	}

	os << "Pattern mean error = " << me << endl;
}



}
