#include "WatkinsQLambdaPatternGenerator.h"

#include <math.h>
#include <iostream>

using namespace std;

namespace brll
{

WatkinsQLambdaPatternGenerator::WatkinsQLambdaPatternGenerator(const QFunction* q, const Policy* p, const RewardFunction* rf, const double & discountFactor, const double & lambda)
	: qFunction(q), policy(p), rf(rf), discountFactor(discountFactor), lambda(lambda)
{
}

WatkinsQLambdaPatternGenerator::~WatkinsQLambdaPatternGenerator()
{
}

QPattern WatkinsQLambdaPatternGenerator::generate(const TrainingData& data) const
{
	QPattern pattern;

	//For each episode
	for (unsigned int i = 0; i < data.getNumEpisodes(); ++i)
	{
//		cout << "Episode " << i+1 << endl;
		const Episode& episode = data.getEpisode(i);

		for (unsigned int j = 0; j < episode.getNumExperiences()-1; ++j) //yes -1, ditto
		{
//			cout << endl << "Experience " << j+1 << endl;
			//experience in time t
			const Experience& currentExp = episode.getExperience(j);

			double sumReward = 0;

			double qValue = 0;

			double sumDecay = 0;

			for (unsigned int k = j+1; k < episode.getNumExperiences(); ++k)
			{
//				cout << "Unroll " << k+1 << endl;
				//experience in time t+k-1
				const Experience& prevExp = episode.getExperience(k-1);

				//experience in time t+k
				const Experience& nextExp = episode.getExperience(k);

				double reward = rf->calcReward(prevExp.getState(), prevExp.getAction(), nextExp.getState());

				sumReward = pow(discountFactor, k-1-j)*reward + sumReward;

				double decay = pow(lambda,k-1-j)*(1-lambda);

				const Action& best = policy->chooseAction(nextExp.getState());

				double qBest = qFunction->getUtility(nextExp.getState(), best);

				double qNext = qFunction->getUtility(nextExp.getState(), nextExp.getAction());

				bool exploratoryAction = (qBest < qNext);

				bool terminal = rf->isStateTerminal(nextExp.getState());

				double tmpReturn;

				if(terminal)
					tmpReturn = sumReward;
				else if(exploratoryAction)
					tmpReturn = sumReward + pow(discountFactor, k-j)*qBest;
				else
					tmpReturn = sumReward + pow(discountFactor, k-j)*qNext;

//				cout << "Reward = " << reward << ", discount = " << decay << endl;

//				cout << "Terminal = " << terminal << endl;

				if((k != episode.getNumExperiences()-1) && (terminal == false) && (exploratoryAction == false))
				{
					sumDecay += decay;
					qValue += decay* tmpReturn;
				}
				else
				{
					qValue += tmpReturn * (1-sumDecay); //remaining to 1
					break;
				}

			}

//			cout << "Added pattern exp = " << j+1 << ", qValue = " << qValue << endl;
			pattern.addPattern(currentExp, qValue);
		}
	}

	return pattern;
}

}
