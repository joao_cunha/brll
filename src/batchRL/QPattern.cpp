#include "QPattern.h"

#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>

namespace brll
{

QPattern::QPattern()
{
}

QPattern::QPattern(string filename, const unsigned & stateDim, const unsigned int & actionDim)
{
	ifstream patternFile;
	patternFile.open(filename.c_str());

	double states[stateDim];
	double actions[actionDim];

	string line;
	while(getline(patternFile, line))
	{
		istringstream ss(line, istringstream::in);
		for(unsigned int i = 0; i < stateDim; ++i)
		{
			double value = -1.0;
			ss >> value;
			states[i] = value;
		}

		for(unsigned int i = 0; i < actionDim; ++i)
		{
			double value = -1.0;
			ss >> value;
			actions[i] = value;
		}

		Experience e = Experience(State(states, stateDim), Action(actions, actionDim));

		ss.ignore(3); // ignoring " | " string
		double output = -1.0;
		ss >> output;
		addPattern(e, output);
	}
}

QPattern::~QPattern()
{
}

unsigned int QPattern::size() const
{
	assert(inputs.size() == qValues.size());
	return inputs.size();
}

const Experience& QPattern::getInputAt(unsigned int i) const
{
	assert(i < inputs.size());
	return inputs.at(i);
}

double QPattern::getOutputAt(unsigned int i) const
{
	assert(i < qValues.size());
	return qValues.at(i);
}

void QPattern::addPattern(const Experience& e, const double & qValue)
{
	inputs.push_back(e);
	qValues.push_back(qValue);
}

void brll::QPattern::setOutputAt(unsigned int i, const double & qValue)
{
	assert(i < qValues.size());
	qValues[i] = qValue;
}

void brll::QPattern::setInputAt(unsigned int i, const Experience& e)
{
	assert(i < inputs.size());
	inputs[i] = e;
}

void QPattern::write(ostream & os) const
{
	for (unsigned int i = 0; i < size(); ++i) //for each pattern
	{
		write(os, i);

		os << endl;
	}
}

void QPattern::write(ostream & os, unsigned int line) const
{
	unsigned int stateDim = getInputAt(0).getState().getDim();
	unsigned int actionDim = getInputAt(0).getAction().getDim();

	for (unsigned int j = 0; j < stateDim; ++j) //for each input value
	{
		os << getInputAt(line).getState().getValue(j) << " ";
	}

	for (unsigned int j = 0; j < actionDim; ++j)
	{
		os << getInputAt(line).getAction().getCommand(j) << " ";
	}

	os << "| " << getOutputAt(line);
}

void QPattern::append(const QPattern& pattern)
{
	for (unsigned int i = 0; i < pattern.size(); ++i)
	{
		addPattern(pattern.getInputAt(i), pattern.getOutputAt(i));
	}
}

} // namespace brll
