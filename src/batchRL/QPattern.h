#ifndef QPATTERN_H_
#define QPATTERN_H_

#include <vector>
#include <iostream>
#include <string>
#include "Experience.h"

using namespace std;

namespace brll
{

class QPattern
{
public:
	QPattern();
	QPattern(string filename, const unsigned & stateDim, const unsigned int & actionDim);
	virtual ~QPattern();
	void addPattern(const Experience& e, const double & qValue);
	unsigned int size() const;
	const Experience& getInputAt(unsigned int i) const;
	void setInputAt(unsigned int i, const Experience& e);
	double getOutputAt(unsigned int i) const;
	void setOutputAt(unsigned int i, const double& qValue);
    void write(ostream & os) const;
    void write(ostream & os, unsigned int line) const;
    void append(const QPattern& pattern);

private:
	vector<Experience> inputs;
	vector<double> qValues;
};

}

#endif /* QPATTERN_H_ */
