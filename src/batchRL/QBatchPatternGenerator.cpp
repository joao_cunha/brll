#include "QBatchPatternGenerator.h"

#include <cmath>
#include <limits>
#include <fstream>

namespace brll
{

QBatchPatternGenerator::QBatchPatternGenerator(const QFunction* q, const Policy* p, const RewardFunction* rf, const double & discountFactor)
	: qFunction(q), policy(p), rf(rf), discountFactor(discountFactor)
{
}

QBatchPatternGenerator::~QBatchPatternGenerator()
{
}

QPattern QBatchPatternGenerator::generate(const TrainingData& data) const
{
	QPattern pattern;
	double qBatchChoices = 0, qLearningChoices = 0;

	//For each episode
	for (unsigned int i = 0; i < data.getNumEpisodes(); ++i)
	{
		const Episode& episode = data.getEpisode(i);

		double prevQmin = std::numeric_limits<float>::max();

		for (int j = episode.getNumExperiences()-2; j >= 0; --j)
		{
			//experience in time t
			const Experience& currentExp = episode.getExperience(j);

			//experience in time t+1
			const Experience& nextExp = episode.getExperience(j+1);

			double qmin;

			double reward = rf->calcReward(currentExp.getState(), currentExp.getAction(), nextExp.getState());

			if(rf->isStateTerminal(nextExp.getState()))
			{
				qmin = reward;
			}
			else
			{
				const Action& best = policy->chooseAction(nextExp.getState());

				double qValue = qFunction->getUtility(nextExp.getState(), best);

				if(prevQmin < qValue)
				{
					qmin = reward + discountFactor*prevQmin;
					qBatchChoices++;
				}
				else
				{
					qmin = reward + discountFactor*qValue;
					qLearningChoices++;
				}
//				qmin = reward + discountFactor*min(prevQmin, qValue);
			}

			prevQmin = qmin;

			pattern.addPattern(currentExp, qmin);
		}

	}

	ofstream qmaxFile;
	qmaxFile.open("qbatch.choices", ofstream::app);
	qmaxFile << qBatchChoices/(qBatchChoices+qLearningChoices) << endl;
	qmaxFile.close();

	return pattern;
}

}  // namespace brll
