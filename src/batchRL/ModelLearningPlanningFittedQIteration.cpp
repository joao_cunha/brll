#include "ModelLearningPlanningFittedQIteration.h"

#include <limits>
#include <math.h>
#include <cassert>

#include "Timer.h"
#include "QTester.h"

namespace brll
{

ModelLearningPlanningFittedQIteration::ModelLearningPlanningFittedQIteration(QFunction* qFunction,
		const QPatternGenerator* generator, TransitionModel* model,
		const Planner* planner, unsigned int iterations, bool test,
		QPattern hintToGoal) :
				FittedQIteration(qFunction, generator, iterations, test, hintToGoal),
		model(model), planner(planner)
{
}

ModelLearningPlanningFittedQIteration::~ModelLearningPlanningFittedQIteration()
{
}

void ModelLearningPlanningFittedQIteration::fit(const TrainingData & data)
{
	Timer timer;

	TransitionPattern transitionPattern = TransitionPattern(data);
	model->train(transitionPattern);
	TrainingData augmentedData = planner->plan(data, model);

	augmentedData.write("brll.plan");

	FittedQIteration::fit(augmentedData);

}

}
