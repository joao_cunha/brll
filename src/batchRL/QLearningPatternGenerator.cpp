#include "QLearningPatternGenerator.h"

namespace brll
{

QLearningPatternGenerator::QLearningPatternGenerator(const QFunction* q, const Policy* p,
	const RewardFunction* rf, const double & discountFactor)
	: qFunction(q), policy(p), rf(rf), discountFactor(discountFactor)
{
}

QLearningPatternGenerator::~QLearningPatternGenerator()
{
}

QPattern QLearningPatternGenerator::generate(const TrainingData& data) const
{
	QPattern pattern;

	//For each episode
	for (unsigned int i = 0; i < data.getNumEpisodes(); ++i)
	{
		const Episode& episode = data.getEpisode(i);

//		std::cout << "Processing episode " << i << ", size = " << episode.getNumExperiences() << std::endl;

		//For each transition in a given episode
		for (unsigned int j = 0; j < episode.getNumExperiences()-1; ++j) //yes, -1 (ditto)
		{
			//experience in time t
			const Experience& currentExp = episode.getExperience(j);

			//experience in time t+1
			const Experience& nextExp = episode.getExperience(j+1);

			//Plain Q-Learning Q(s_t,a_t) = r_{t+1} + max_{a'} Q(s_{t+1},a')
			double qValue;

			double reward = rf->calcReward(currentExp.getState(), currentExp.getAction(), nextExp.getState());
			if(rf->isStateTerminal(nextExp.getState()))
			{
				qValue = reward;
			}
			else
			{
				const Action& best = policy->chooseAction(nextExp.getState());
				qValue = reward + discountFactor * qFunction->getUtility(nextExp.getState(), best);
			}

			pattern.addPattern(currentExp, qValue);
		}
	}

	return pattern;
}

}  // namespace brll
