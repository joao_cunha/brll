#ifndef QPATTERNGENERATOR_H_
#define QPATTERNGENERATOR_H_

#include "QPattern.h"
#include "TrainingData.h"

namespace brll
{

class QPatternGenerator
{
public:
	virtual ~QPatternGenerator() {};
	virtual QPattern generate(const TrainingData& data) const = 0;
};

}

#endif /* QPATTERNGENERATOR_H_ */
