#ifndef QTESTER_H_
#define QTESTER_H_

#include <fstream>
#include <vector>

#include "QPattern.h"
#include "QFunction.h"

using namespace std;

namespace brll
{

class QTester
{
public:
	QTester(const QFunction* qFunction, const QPattern& pattern);
	virtual ~QTester();
	double meanError();
	void write(ostream & os);

private:
	const QFunction* qFunction;
	QPattern pattern;
	vector<double> outputs;

};

}

#endif /* QTESTER_H_ */
