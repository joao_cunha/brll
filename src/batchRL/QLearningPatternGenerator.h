#ifndef QLEARNINGPATTERNGENERATOR_H_
#define QLEARNINGPATTERNGENERATOR_H_

#include <string>

#include "QPatternGenerator.h"
#include "QFunction.h"
#include "Policy.h"
#include "RewardFunction.h"

namespace brll
{

class QLearningPatternGenerator: public QPatternGenerator
{
public:
	QLearningPatternGenerator(const QFunction* q, const Policy* p, const RewardFunction* rf, const double & discountFactor);
	virtual ~QLearningPatternGenerator();
	virtual QPattern generate(const TrainingData& data) const;

private:
	const QFunction* qFunction;
	const Policy* policy;
	const RewardFunction* rf;
	const double discountFactor;

};

}  // namespace brll

#endif /* QLEARNINGPATTERNGENERATOR_H_ */
