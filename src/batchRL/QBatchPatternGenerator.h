#ifndef QBATCHPATTERNGENERATOR_H_
#define QBATCHPATTERNGENERATOR_H_

#include "QPatternGenerator.h"
#include "QFunction.h"
#include "Policy.h"
#include "RewardFunction.h"

namespace brll
{

class QBatchPatternGenerator: public QPatternGenerator
{
public:
	QBatchPatternGenerator(const QFunction* q, const Policy* p, const RewardFunction* rf, const double & discountFactor);
	virtual ~QBatchPatternGenerator();
	virtual QPattern generate(const TrainingData& data) const;

private:
	const QFunction* qFunction;
	const Policy* policy;
	const RewardFunction* rf;
	const double discountFactor;
};

}  // namespace brll

#endif /* QBATCHPATTERNGENERATOR_H_ */
