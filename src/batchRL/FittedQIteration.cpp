/*
 * FittedQIteration.cpp
 *
 *  Created on: Feb 7, 2013
 *      Author: Joao Cunha <joao.cunha@ua.pt>
 */

#include "FittedQIteration.h"

#include <fstream>
#include <limits>
#include <math.h>


#include "Timer.h"
#include "QTester.h"

#include <cassert>

namespace brll
{

FittedQIteration::FittedQIteration(QFunction* qFunction, const QPatternGenerator* generator,
	unsigned int iterations, bool test, QPattern hintToGoal) :
	qFunction(qFunction), patternGenerator(generator), iterations(iterations), test(test), hintToGoal(hintToGoal)
{
}

FittedQIteration::~FittedQIteration()
{
	delete patternGenerator;
}

void FittedQIteration::fit(const TrainingData& data)
{

	Timer timer;
	for (unsigned int i = 0; i < iterations; ++i)
	{
		cout << "Inner Iteration #" << i << "..." << endl;

		//generate pattern
		QPattern pattern = patternGenerator->generate(data);

		for (unsigned int i = 0; i < preProcess.size(); ++i)
			preProcess.at(i)->process(pattern);


		//temporary pattern to keep hint-to-goal in the beginning
		QPattern tmpPat;

		//add hint-to-goal
		tmpPat.append(hintToGoal);

		//add "real" pattern
		tmpPat.append(pattern);

		ofstream patternFile;
		patternFile.open("brll.pat", ofstream::trunc);
		tmpPat.write(patternFile);
		patternFile.close();

		qFunction->train(tmpPat);

		if(test)
		{
			ofstream testFile;
			testFile.open("brll.test", ofstream::trunc);
			QTester tester = QTester(qFunction, tmpPat);
			tester.write(testFile);
			testFile.close();
		}

	}

	unsigned int ms = timer.elapsed();

	ofstream timeFile;
	timeFile.open("brll.time", ofstream::app);
	timeFile << ms << endl;
	timeFile.close();
}

void FittedQIteration::addPreProcess(PreProcess* p)
{
	preProcess.push_back(p);
}

}
