#ifndef WATKINSQLAMBDAPATTERNGENERATOR_H_
#define WATKINSQLAMBDAPATTERNGENERATOR_H_

#include "QPatternGenerator.h"
#include "QFunction.h"
#include "Policy.h"
#include "RewardFunction.h"

namespace brll
{

class WatkinsQLambdaPatternGenerator: public brll::QPatternGenerator
{
public:
	WatkinsQLambdaPatternGenerator(const QFunction* q, const Policy* p, const RewardFunction* rf, const double & discountFactor, const double & lambda);
	virtual ~WatkinsQLambdaPatternGenerator();
	virtual QPattern generate(const TrainingData& data) const;

private:
	const QFunction* qFunction;
	const Policy* policy;
	const RewardFunction* rf;
	const double discountFactor;
	const double lambda;
};

}

#endif /* WATKINSQLAMBDAPATTERNGENERATOR_H_ */
