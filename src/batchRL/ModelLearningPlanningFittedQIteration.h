#ifndef MODELLEARNINGPLANNINGFITTEDQITERATION_H_
#define MODELLEARNINGPLANNINGFITTEDQITERATION_H_

#include "FittedQIteration.h"
#include "TransitionModel.h"
#include "Planner.h"
#include "TrainingData.h"

namespace brll
{

class ModelLearningPlanningFittedQIteration : public FittedQIteration
{
public:
	ModelLearningPlanningFittedQIteration(QFunction* qFunction,
			const QPatternGenerator* generator, TransitionModel* model,
			const Planner* planner, unsigned int iterations, bool test, QPattern hintToGoal = QPattern());
	virtual ~ModelLearningPlanningFittedQIteration();
	virtual void fit(const TrainingData& data);

private:
	TransitionModel* model;
	const Planner* planner;
};

}

#endif /* MODELLEARNINGPLANNINGFITTEDQITERATION_H_ */
