#include "Timer.h"

#include "Clock.h"

#include <sys/time.h>
#include <time.h>
#include <stdio.h>

namespace brll
{

Timer::Timer()
{
	restart();
}

void Timer::restart()
{

	initTime = Clock::now();
}

unsigned int Timer::elapsed()
{
	return (Clock::now() - initTime);
}

}  // namespace brll
