#ifndef _GPLOT_H_
#define _GPLOT_H_

#include <stdio.h>
#include <stdarg.h>
#include <string>
#include <vector>

using namespace std;

class GPlot {

 protected:
  FILE   *gplotPipe;
  bool    valid;

  int nplots;

  bool init(void);

 public:
  GPlot(void);
  ~GPlot(void);

  void pipeCmd(const char*);
  void pipeCmdStr(const char*, ...);

  void setLabels(string title, string x, string y);

  void plot(const vector<double> &v, string title="", string style="", double scale=1);
  void plot(const vector<double> &u,const vector<double> &v, string title="", string style="", double scale=1);
  void plot(string filename, int row2=1, int row1=0, int index = -1, string title="", string style="", double scale=1);

  void reset(void);
};

#endif
