#include "Clock.h"

#include <sys/time.h>
#include <time.h>

namespace brll
{

Clock::Clock()
{
}

Clock::~Clock()
{
}

unsigned int Clock::now()
{
	struct timeval tv;
	gettimeofday( &tv , NULL );
	return tv.tv_sec*1000+tv.tv_usec/1000;
}

}  // namespace brll

