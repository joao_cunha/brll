#include "Statistics.h"

#include <fstream>
#include <iostream>

using namespace std;

namespace brll
{

Statistics::Statistics(string filename, const RewardFunction* rf)
	: filename(filename), rf(rf)
{
	double dummy [] = {0.0};
	currentS = State(dummy, 1);
	currentA = Action(dummy, 1);

	rewardSum = 0;
	numCycles = 0;
	sPlusPercentage = 0;
	sMinusPercentage = 0;
	sWorkPercentage = 0;
	outcome = 0;
}

Statistics::~Statistics()
{
	delete rf;
}

void Statistics::update(const State& s, const Action& a)
{
	if(numCycles > 0)
	{

		double reward = rf->calcReward(currentS, currentA, s);

		rewardSum += reward;
		if(rf->isStateInSPlus(s))
		{
			sPlusPercentage++;
			outcome = 1;
		}
		else if(rf->isStateInSMinus(s))
		{
			sMinusPercentage++;
			outcome = -1;
		}
		else
		{
			sWorkPercentage++;
			outcome = 0;
		}
	}
	numCycles++;
	currentS = s;
	currentA = a;
}

void Statistics::flush()
{
	if(numCycles > 1)
	{
		numCycles--;	//remove first cycle
		sPlusPercentage /= numCycles;
		sMinusPercentage /= numCycles;
		sWorkPercentage /= numCycles;

		ofstream ofs(filename.c_str(), ofstream::app);

		ofs << numCycles << " " << rewardSum << " " << sPlusPercentage << " " << sMinusPercentage
				 << " " << sWorkPercentage << " " << outcome << endl;
	}

	rewardSum = 0;
	numCycles = 0;
	sPlusPercentage = 0;
	sMinusPercentage = 0;
	sWorkPercentage = 0;
	outcome = 0;
}

}  // namespace brll
