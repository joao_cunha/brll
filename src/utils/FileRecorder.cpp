#include "FileRecorder.h"

#include <fstream>
#include <stdlib.h>
#include <iostream>

namespace brll
{

FileRecorder::FileRecorder(const string& filename)
	: filename(filename), run(Episode())
{
}

FileRecorder::~FileRecorder()
{
}

void FileRecorder::record(const State& s, const Action& a)
{
		run.addExperience(Experience(s, a));
}

void FileRecorder::flush()
{
	if(run.getNumExperiences() < 2) //if less than 2 then no transitions
	{
		run = Episode();
		return;
	}

	//open file to write
	ofstream dataFile;
	dataFile.open((const char*)filename.c_str(), ofstream::app); //open with append
	dataFile << "---" << endl;
	for (unsigned int i = 0; i < run.getNumExperiences(); ++i) //for each Experience
	{
		const Experience& e = run.getExperience(i);
		const State& s = e.getState();
		const Action& a = e.getAction();
		for (unsigned int j = 0; j < s.getDim(); ++j)
		{
			dataFile << s.getValue(j) << " ";
		}
		for (unsigned int j = 0; j < a.getDim(); ++j)
		{
			dataFile << a.getCommand(j) << " ";
		}
		dataFile << endl;
	}

	dataFile.close();

	run = Episode();
}

}  // namespace brll
