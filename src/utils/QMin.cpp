#include "QMin.h"

#include <cassert>
#include <limits>
#include <math.h>

namespace brll
{

QMin::QMin()
{
}

QMin::~QMin()
{
}

void QMin::process(QPattern& pattern)
{
	double minValue = std::numeric_limits<double>::max();
	double maxValue = -std::numeric_limits<double>::max();
	for (unsigned int i = 0; i < pattern.size(); ++i)
	{
		double value = pattern.getOutputAt(i);
		minValue = min(minValue, value);
		maxValue = max(maxValue, value);
	}
	if(maxValue - minValue > 0.001 && minValue > 0)
		//subtract minimum Q value to all targets
		for (unsigned int i = 0; i < pattern.size(); ++i)
		{
			assert(pattern.getOutputAt(i) - minValue >= 0);
			pattern.setOutputAt(i, pattern.getOutputAt(i) - minValue);
		}
}

} /* namespace brll */
