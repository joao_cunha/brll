#ifndef QMIN_H_
#define QMIN_H_

#include "PreProcess.h"

namespace brll
{

class QMin: public PreProcess
{
public:
	QMin();
	virtual ~QMin();
	virtual void process(QPattern& pattern);
};

} /* namespace brll */
#endif /* QMIN_H_ */
