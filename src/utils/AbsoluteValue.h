#ifndef ABSOLUTEVALUE_H_
#define ABSOLUTEVALUE_H_

#include "PreProcess.h"

namespace brll
{

class AbsoluteValue: public brll::PreProcess
{
public:
	AbsoluteValue();
	virtual ~AbsoluteValue();
	virtual void process(QPattern& pattern);
};

} /* namespace brll */
#endif /* ABSOLUTEVALUE_H_ */
