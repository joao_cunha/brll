#include "AbsoluteValue.h"

#include <math.h>

namespace brll
{

AbsoluteValue::AbsoluteValue()
{
}

AbsoluteValue::~AbsoluteValue()
{
}

void AbsoluteValue::process(QPattern& pattern)
{
		for (unsigned int i = 0; i < pattern.size(); ++i)
		{
			double value = pattern.getOutputAt(i);
			if(value < 0)
			{
				value = fabs(value);
				pattern.setOutputAt(i, value);
			}
		}
}

} /* namespace brll */
