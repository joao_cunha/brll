#ifndef MODULE_H_
#define MODULE_H_

#include "State.h"
#include "Action.h"

namespace brll
{

class Module
{
public:
	virtual ~Module(){};
	virtual void update(const State& s, const Action& a) = 0;
	virtual void flush()=0;
};

} /* namespace brll */
#endif /* MODULE_H_ */
