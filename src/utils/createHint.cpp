#include <iostream>

#include <vector>
#include <stdlib.h>
#include <cmath>
#include <fstream>

#include "Clock.h"
#include "QPattern.h"
#include "State.h"
#include "Action.h"
#include "Experience.h"

using namespace std;
using namespace brll;

int main(void)
{


	srand48(Clock::now());

	unsigned int stateDim;

	cout << "State Dim: ";
	cin >> stateDim;

	double state[stateDim];
	double range[stateDim];

	for (unsigned int i = 0; i < stateDim; ++i)
	{
		cout << "State[" << i << "] = ";
		cin >> state[i];
		cout << "Range: ";
		cin >> range[i];
	}

	unsigned int numActions;

	cout << "Number of actions: ";
	cin >> numActions;

	unsigned int actionDim;
	cout << "Action Dim: ";
	cin >> actionDim;

	vector<Action> actions;


	for (unsigned int i = 0; i < numActions; ++i)
	{
		double actionTmp[actionDim];
		cout << "Action #" << i << ":" << endl;
		for (unsigned int j = 0; j < actionDim; ++j)
		{
			cout << "Dim " << j << ": ";
			cin >> actionTmp[j];
		}
		actions.push_back(Action(actionTmp, actionDim));
	}

	unsigned int numPatterns;
	cout << "Number of patterns: ";
	cin >> numPatterns;

	QPattern pattern;

	for (unsigned int i = 0; i < numPatterns; ++i)
	{
		double tmpState[stateDim];
		for (unsigned int j = 0; j < stateDim; ++j)
		{
			tmpState[j] = state[j];
			if(i != 0)
				tmpState[j] += drand48()*2*range[j] - range[j];
		}

		State s = State(tmpState, stateDim);

		for (unsigned int j = 0; j < numActions; ++j)
		{
			Action a = actions[j];
			Experience e = Experience(s,a);
			pattern.addPattern(e, 0);
		}
	}

	string filename;
	cout << "Filename: ";
	cin >> filename;

	ofstream ofs;
	ofs.open(filename.c_str(), ofstream::app);

	pattern.write(ofs);

	return 0;
}
