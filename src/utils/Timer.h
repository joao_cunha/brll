#ifndef TIMER_H_
#define TIMER_H_

namespace brll
{

class Timer
{
	unsigned int initTime;
	
public:
	Timer();
	
	void restart();
	unsigned int elapsed();
};


}  // namespace brll

#endif // TIMER_H_
