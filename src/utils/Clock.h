#ifndef CLOCK_H_
#define CLOCK_H_

namespace brll
{

class Clock
{
public:
	Clock();
	virtual ~Clock();
	static unsigned int now();
};

}  // namespace brll

#endif /* CLOCK_H_ */
