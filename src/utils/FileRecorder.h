#ifndef FILE_RECORDER_H_
#define FILE_RECORDER_H_

#include "Module.h"
#include "Episode.h"
#include "State.h"
#include "Action.h"
#include <string>

using namespace std;

namespace brll
{

class FileRecorder : public Module
{
public:
	FileRecorder(const string& filename);
	virtual ~FileRecorder();
	virtual void record(const State& s, const Action& a);
	virtual void flush();

private:
	const string filename;
	Episode run;
};

}  // namespace brll

#endif /* FILE_RECORDER_H_ */
