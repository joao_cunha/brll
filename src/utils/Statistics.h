#ifndef STATISTICS_H_
#define STATISTICS_H_

#include "Module.h"
#include "RewardFunction.h"
#include "State.h"
#include "Action.h"

#include <string>

using namespace std;

namespace brll
{

class Statistics : public Module
{
public:
	Statistics(string filename, const RewardFunction* rf);
	virtual ~Statistics();
	virtual void update(const State& s, const Action& a);
	virtual void flush();

private:
	string filename;
	const RewardFunction* rf;
	State currentS;
	Action currentA;

	double rewardSum;
	int numCycles;
	double sPlusPercentage;
	double sMinusPercentage;
	double sWorkPercentage;
	int outcome;
};

}  // namespace brll

#endif /* STATISTICS_H_ */
