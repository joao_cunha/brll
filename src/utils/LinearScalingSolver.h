#ifndef LINEARSCALINGSOLVER_H_
#define LINEARSCALINGSOLVER_H_

namespace brll
{

class LinearScalingSolver
{
public:
	LinearScalingSolver();
	virtual ~LinearScalingSolver();

	void solve(const double& minIn, const double& maxIn, const double& minOut, const double& maxOut);

	double A,B;
};

}  // namespace brll

#endif /* LINEARSCALINGSOLVER_H_ */
