#ifndef PREPROCESS_H_
#define PREPROCESS_H_

#include "QPattern.h"

namespace brll
{

class PreProcess
{
public:
	virtual ~PreProcess() {};
	virtual void process(QPattern& pattern) = 0;
};

} /* namespace brll */
#endif /* PREPROCESS_H_ */
