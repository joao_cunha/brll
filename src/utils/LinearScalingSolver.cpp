#include "LinearScalingSolver.h"

namespace brll
{

LinearScalingSolver::LinearScalingSolver()
{
	A = 0;
	B = 0;
}

LinearScalingSolver::~LinearScalingSolver()
{
}

void LinearScalingSolver::solve(const double& minIn, const double& maxIn, const double& minOut, const double& maxOut)
{
	if((maxIn-minIn) > 0.0001)
	{
		A = (maxOut - minOut) / (maxIn - minIn);
		B = (- (minIn * (maxOut - minOut)) / (maxIn - minIn)) + minOut;
	}
	else
	{
		A = 0;
		B = maxIn/2;
	}
}

}  // namespace brll
