#ifndef NEURALQFUNCTION_H_
#define NEURALQFUNCTION_H_

#include "QFunction.h"
#include "n++/n++.h"
#include "NNBatchTrainer.h"

namespace brll
{

class NeuralQFunction: public QFunction
{
public:
	NeuralQFunction(Net* neural_net, const NNBatchTrainer& trainer = NNBatchTrainer());
	virtual ~NeuralQFunction();
	virtual double getUtility(const State& s, const Action& a) const;
	virtual void train(const QPattern& pattern);

private:
	Net* neural_net;
	NNBatchTrainer trainer;

};

}  // namespace brll

#endif /* NEURALQFUNCTION_H_ */
