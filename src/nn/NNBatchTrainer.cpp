#include "NNBatchTrainer.h"

#include <vector>
#include <limits>

#include "Clock.h"
#include "n++/aux.h"
#include "gplot.h"
#include "LinearScalingSolver.h"

namespace brll
{

NNBatchTrainer::NNBatchTrainer()
	: epochs(0), scalingInputs(false), scalingOutputs(false), plot(false)
{
}

NNBatchTrainer::NNBatchTrainer(const unsigned int & epochs, const bool & scalingInputs, const bool & scalingOutputs, const bool & plot)
	: epochs(epochs), scalingInputs(scalingInputs), scalingOutputs(scalingOutputs), plot(plot)
{
}

NNBatchTrainer::~NNBatchTrainer()
{
}

void NNBatchTrainer::train(Net* neural_net, PatternSet & pattern)
{
	if (scalingInputs || scalingOutputs)
		scale(neural_net, pattern);

	neural_net->set_seed(Clock::now());
	neural_net->init_weights(0, 1);

	float uparams[] = { 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
		0.000000, 0.000000, 0.000000 }; //default values will be set
	neural_net->set_update_f(RPROP, uparams);

	aux_params.update_mode = OFFLINE;
	aux_params.tolerance = 1;

	std::vector<double> errors;

	result_type error;
	for (unsigned int i = 0; i < epochs; ++i)
	{
		aux_trainAll(neural_net, &pattern, &error);
		double mse = error.tss / pattern.pattern_count;
		errors.push_back(mse);
	}

	if (plot)
	{
		GPlot gplot;
		gplot.reset();
		gplot.setLabels("Error by epoch", "Epoch", "Mean Squared Error");
		gplot.plot(errors, "mean squared error", "l lt 1");
		sleep(3);
	}
}

void NNBatchTrainer::scale(Net* neural_net, const PatternSet & pattern)
{
	unsigned int inputDim = pattern.input_count;
	vector<double> minInputs(inputDim, std::numeric_limits<float>::max());
	vector<double> maxInputs(inputDim, -std::numeric_limits<float>::max());

	unsigned int outputDim = pattern.target_count;
	vector<double> minOutputs(outputDim, std::numeric_limits<float>::max());
	vector<double> maxOutputs(outputDim, -std::numeric_limits<float>::max());

	for (int i = 0; i < pattern.pattern_count; ++i)
	{
		for (unsigned int j = 0; j < inputDim; ++j)
		{
			minInputs[j] = min(minInputs[j], static_cast<double>(pattern.input[i][j]));
			maxInputs[j] = max(maxInputs[j], static_cast<double>(pattern.input[i][j]));
		}

		for (unsigned int j = 0; j < outputDim; ++j)
		{
			minOutputs[j] = min(minOutputs[j], static_cast<double>(pattern.target[i][j]));
			maxOutputs[j] = max(maxOutputs[j], static_cast<double>(pattern.target[i][j]));
		}
	}

	LinearScalingSolver linSolver;

	if(scalingInputs)
	{
		for (unsigned int i = 0; i < inputDim; ++i)
		{
			linSolver.solve(minInputs[i], maxInputs[i], -1, 1); //inputs scaled from [min..max] to [-1..1]
			neural_net->insert_scale_element(&neural_net->scale_list_in, i + 1, 1, linSolver.A,
				linSolver.B, 0);
		}
	}

	if(scalingOutputs)
	{
		for (unsigned int i = 0; i < outputDim; ++i)
		{
			linSolver.solve(0, 1, minOutputs[i], maxOutputs[i]); //output scaled from [0..1] to [min..max]
			neural_net->insert_scale_element(&neural_net->scale_list_out, i + 1, 1, linSolver.A,
				linSolver.B, 0);
		}
	}


}

}
