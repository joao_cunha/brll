#ifndef NNBATCHTRAINER_H_
#define NNBATCHTRAINER_H_

#include "n++/n++.h"
#include "n++/PatternSet.h"

namespace brll
{

class NNBatchTrainer
{
public:
	NNBatchTrainer();
	NNBatchTrainer(const unsigned int & epochs, const bool & scalingInputs, const bool & scalingOutputs, const bool & plot);
	virtual ~NNBatchTrainer();
	void train(Net* neural_net, PatternSet & pattern);

private:
	void scale(Net* neural_net, const PatternSet & pattern);

private:
	const unsigned int epochs;
	const bool scalingInputs, scalingOutputs;
	const bool plot;
};

}

#endif /* NNBATCHTRAINER_H_ */
