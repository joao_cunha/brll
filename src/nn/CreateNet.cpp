#include <iostream>
#include <sys/time.h>

#include "n++/n++.h"
#include "Clock.h"

using namespace std;
using namespace brll;

int main(int argc, char **argv)
{

	if(argc < 5)
	{
		cout << "Usage: " << argv[0] << " <netfilename> <#layers> <layer_1> <layer_2> ... <layer_n> "<< endl;
		exit(1);
	}

	int nLayers = atoi(argv[2]);

	if(nLayers < 0)
	{
		cout << "Invalid number of layers : " << argv[2] << " . Expected a positive integer." << endl;
		exit(1);
	}
	else if((argc - 3) != nLayers )
	{
		cout << "Number of provided layers, " << nLayers << ", inconsistent with layers in input arguments, " << argc-3 << "." << endl;
		exit(1);
	}

	int layers[nLayers];

	for (unsigned int i = 0; i < (unsigned int)nLayers; ++i)
	{
		layers[i] = atoi(argv[i + 3]);
	}

	// feed-forward neural network
	Net net;
	net.create_layers(nLayers,layers);
	net.connect_layers();

	net.set_seed(Clock::now());
	net.init_weights(0,1);

	float uparams[] = {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000}; //default values will be set

	net.set_update_f(RPROP,uparams);

	for (int i = 1; i < nLayers; ++i)
	{
		net.set_layer_act_f(i,LOGISTIC);
	}

	net.print_net();

	net.save_net(argv[1]);
	cout << "Net saved in file " << argv[1] << endl;

}
