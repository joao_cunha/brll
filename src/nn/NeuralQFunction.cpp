#include "NeuralQFunction.h"

#include "Clock.h"

#include "n++/PatternSet.h"

namespace brll
{

NeuralQFunction::NeuralQFunction(Net* neural_net, const NNBatchTrainer& trainer)
	: neural_net(neural_net), trainer(trainer)
{
}

NeuralQFunction::~NeuralQFunction()
{
	delete neural_net;
}

double NeuralQFunction::getUtility(const State& s, const Action& a) const
{
	float inputs[s.getDim() + a.getDim()];
	float output[1]; // Q-Function only outputs a single value

	for (unsigned int i = 0; i < s.getDim(); ++i)
	{
		inputs[i] = s.getValue(i);
	}

	for (unsigned int i = s.getDim(); i < s.getDim()+a.getDim(); ++i)
	{
		inputs[i] = a.getCommand(i - s.getDim());
	}

	neural_net->forward_pass(inputs, output);
	return static_cast<double>(output[0]); //single output neuron
}

void NeuralQFunction::train(const QPattern& pattern)
{
	unsigned int stateDim = pattern.getInputAt(0).getState().getDim();
	unsigned int actionDim = pattern.getInputAt(0).getAction().getDim();

	//build pattern compatible with n++
	PatternSet pat;
	pat.create(pattern.size(), stateDim + actionDim, 1); //Q-value has a single output

	float in[stateDim + actionDim];
	float out[1];

	for (unsigned int i = 0; i < pattern.size(); ++i)
	{
		for (unsigned int j = 0; j < stateDim; ++j)
			in[j] = pattern.getInputAt(i).getState().getValue(j);

		for (unsigned int j = 0; j < actionDim; ++j)
			in[stateDim + j] = pattern.getInputAt(i).getAction().getCommand(j);

		out[0] = pattern.getOutputAt(i);

		pat.add_pattern(in, out);
	}

	trainer.train(neural_net, pat);
}

}  // namespace brll
