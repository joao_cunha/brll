#ifndef TRANSITIONPATTERN_H_
#define TRANSITIONPATTERN_H_

#include <vector>

#include "StateTransition.h"
#include "TrainingData.h"

namespace brll
{

class TransitionPattern
{
public:
	TransitionPattern();
	TransitionPattern(const TrainingData& data);
	virtual ~TransitionPattern();
	void addTransition(const StateTransition& t);
	const StateTransition& getTransition(unsigned int i) const;
	unsigned int size() const;
	void write(ostream & os);


private:
	std::vector<StateTransition> transitions;
};

}

#endif /* TRANSITIONPATTERN_H_ */
