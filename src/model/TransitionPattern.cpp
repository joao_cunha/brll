#include "TransitionPattern.h"

#include <cassert>

#include "State.h"
#include "Action.h"

namespace brll
{

TransitionPattern::TransitionPattern()
{
}

TransitionPattern::TransitionPattern(const TrainingData& data)
{
	unsigned int stateDim = data.getEpisode(0).getExperience(0).getState().getDim();
	double deltas[stateDim];

	for (unsigned int i = 0; i < data.getNumEpisodes(); ++i)
	{
		const Episode& episode = data.getEpisode(i);

		for (unsigned int j = 0; j < episode.getNumExperiences() - 1; ++j)
		{
			const Experience& currentExp = episode.getExperience(j);
			const Experience& nextExp = episode.getExperience(j+1);

			for (unsigned int k = 0; k < stateDim; ++k)
			{
				deltas[k] = nextExp.getState().getValue(k) - currentExp.getState().getValue(k);
			}

			State delta = State(deltas, stateDim);

			StateTransition t = StateTransition(currentExp.getState(), currentExp.getAction(), delta);
			addTransition(t);
		}
	}
}

TransitionPattern::~TransitionPattern()
{
//	for (unsigned int i = 0; i < transitions.size(); ++i)
//	{
//		delete transitions.at(i).getDelta();
//	}
}

void TransitionPattern::addTransition(const StateTransition& t)
{
	transitions.push_back(t);
}

const StateTransition& TransitionPattern::getTransition(unsigned int i) const
{
	assert(i < size());
	return transitions.at(i);
}

unsigned int TransitionPattern::size() const
{
	return transitions.size();
}

void TransitionPattern::write(ostream & os)
{
	unsigned int stateDim = transitions.at(0).getState().getDim();
	unsigned int actionDim = transitions.at(0).getAction().getDim();

	for (unsigned int i = 0; i < size(); ++i)
	{
		const StateTransition& t = transitions.at(i);

		const State& s = t.getState();

		for (unsigned int j = 0; j < stateDim; ++j)
		{
			os << s.getValue(j) << " ";
		}

		const Action& a = t.getAction();

		for (unsigned int j = 0; j < actionDim; ++j)
		{
			os << a.getCommand(j) << " ";
		}

		const State& delta = t.getDelta();

		for (unsigned int j = 0; j < stateDim; ++j)
		{
			os << delta.getValue(j) << " ";
		}

		os << std::endl;
	}
}

}
