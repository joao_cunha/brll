#include "StateTransition.h"

namespace brll
{

StateTransition::StateTransition(const State& s, const Action& a, const State& delta)
	: s(s), a(a), delta(delta)
{
}

StateTransition::~StateTransition()
{
}

const State& StateTransition::getState() const
{
	return s;
}

const Action& StateTransition::getAction() const
{
	return a;
}

const State& StateTransition::getDelta() const
{
	return delta;
}

State StateTransition::getNextState() const
{

	double next[s.getDim()];

	for (unsigned int i = 0; i < s.getDim(); ++i)
		next[i] = s.getValue(i) + delta.getValue(i);

	State nextState = State(next, s.getDim());

	return nextState;
}

}
