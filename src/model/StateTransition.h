#ifndef STATETRANSITION_H_
#define STATETRANSITION_H_

#include "State.h"
#include "Action.h"

namespace brll
{

class StateTransition
{
public:
	StateTransition(const State& s, const Action& a, const State& delta);
	virtual ~StateTransition();

	const State& getState() const;
	const Action& getAction() const;
	const State& getDelta() const;
	State getNextState() const;

private:
	State s;
	Action a;
	State delta;
};

}

#endif /* STATETRANSITION_H_ */
