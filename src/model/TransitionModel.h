#ifndef TRANSITIONMODEL_H_
#define TRANSITIONMODEL_H_

#include "State.h"
#include "Action.h"
#include "TransitionPattern.h"

namespace brll
{

class TransitionModel
{
public:
	virtual ~TransitionModel(){};
	virtual State mean(const State& s, const Action& a) const = 0;
	virtual State sigma(const State& s, const Action& a) const = 0;
	virtual State sample(const State& s, const Action& a) const = 0;
	virtual void train(const TransitionPattern& pattern) = 0;
};

}

#endif /* TRANSITIONMODEL_H_ */
