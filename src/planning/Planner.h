#ifndef PLANNER_H_
#define PLANNER_H_

#include "TrainingData.h"
#include "TransitionModel.h"

namespace brll
{

class Planner
{
public:
	virtual ~Planner() {};
	virtual TrainingData plan(const TrainingData & data, const TransitionModel * model) const = 0;
};

}

#endif /* PLANNER_H_ */
