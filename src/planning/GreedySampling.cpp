#include "GreedySampling.h"

namespace brll
{

GreedySampling::GreedySampling(const Policy* pi, const ActionSet & actions)
	: pi(pi), actions(actions)
{
}

GreedySampling::~GreedySampling()
{
}

TrainingData GreedySampling::plan(const TrainingData& data,	const TransitionModel* model) const
{
	TrainingData augmentedData = TrainingData(data.getStateDim(), data.getActionDim());

	std::cout << "Planning" << std::endl;

	for (unsigned int i = 0; i < data.getNumEpisodes(); ++i)
	{
		const Episode & e = data.getEpisode(i);

//		std::cout << "Processing episode " << i << ", size = " << e.getNumExperiences() << std::endl;

		augmentedData.addEpisode(e); //add real episode to the data

		Episode imaginary;

		bool exploring = false;

		State s = e.getExperience(0).getState();

		for (unsigned int j = 0; j < e.getNumExperiences()-1; ++j)
		{

			Action a = e.getExperience(j).getAction();
			State sNext = e.getExperience(j+1).getState();

			if( (a != pi->chooseAction(s)) && !exploring)
				exploring = true;

			if(exploring)
			{
				a = pi->chooseAction(s);
				sNext = model->sample(s,a);
			}

			imaginary.addExperience(Experience(s,a));

			if(j == e.getNumExperiences()-1)
				imaginary.addExperience(Experience(sNext, a)); //dummy action

			s = sNext;
		}

		if(exploring)
			augmentedData.addEpisode(imaginary);
	}

	return augmentedData;


}

} /* namespace brll */
