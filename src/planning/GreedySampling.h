#ifndef GREEDYSAMPLING_H_
#define GREEDYSAMPLING_H_

#include "Planner.h"
#include "ActionSet.h"
#include "Policy.h"

namespace brll
{

class GreedySampling : public Planner
{
public:
	GreedySampling(const Policy* pi, const ActionSet & actions);
	virtual ~GreedySampling();
	virtual TrainingData plan(const TrainingData & data, const TransitionModel * model) const;

private:
	const Policy* pi;
	ActionSet actions;

};

} /* namespace brll */
#endif /* GREEDYSAMPLING_H_ */
