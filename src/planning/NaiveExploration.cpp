#include "NaiveExploration.h"

#include <iostream>

namespace brll
{

NaiveExploration::NaiveExploration(const ActionSet& actions)
	: actions(actions)
{
}

NaiveExploration::~NaiveExploration()
{
}

TrainingData NaiveExploration::plan(const TrainingData & data, const TransitionModel * model) const
{
	TrainingData augmentedData = TrainingData(data.getStateDim(), data.getActionDim());

	std::cout << "Planning" << std::endl;

	for (unsigned int i = 0; i < data.getNumEpisodes(); ++i)
	{
		const Episode & e = data.getEpisode(i);

//		std::cout << "Processing episode " << i << ", size = " << e.getNumExperiences() << std::endl;

		augmentedData.addEpisode(e); //add real episode to the data

		Episode basis; //basis will incrementaly store the real trajectory until time step j

		for (unsigned int j = 0; j < e.getNumExperiences()-1; ++j)
		{
//			std::cout << "\tProcessing experience " << j << std::endl;

			for (unsigned int k = 0; k < actions.getNumActions(); ++k)
			{
				//copy basis
				Episode imaginary; // = Episode(basis);

				const Action & a = actions.getAction(k);

				if(a != e.getExperience(j).getAction()) //compare with taken action
				{
					State s = model->sample(e.getExperience(j).getState(), a);
					imaginary.addExperience(Experience(e.getExperience(j).getState(), a));
					imaginary.addExperience(Experience(s, a)); //dummy action!
					augmentedData.addEpisode(imaginary);
				}
			}
			//add experience at time step j
			basis.addExperience(e.getExperience(j));
		}
	}

	for (unsigned int i = 0; i < data.getNumEpisodes(); ++i)
		augmentedData.addEpisode(data.getEpisode(i)); //add real episode to the data

	return augmentedData;
}

}
