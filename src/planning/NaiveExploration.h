#ifndef NAIVEEXPLORATION_H_
#define NAIVEEXPLORATION_H_

#include "Planner.h"
#include "ActionSet.h"

namespace brll
{

class NaiveExploration : public Planner
{
public:
	NaiveExploration(const ActionSet & actions);
	virtual ~NaiveExploration();
	virtual TrainingData plan(const TrainingData & data, const TransitionModel * model) const;

private:
	ActionSet actions;
};

}

#endif /* NAIVEEXPLORATION_H_ */
