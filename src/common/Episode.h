#ifndef EPISODE_H_
#define EPISODE_H_

#include <deque>
#include "Experience.h"

using namespace std;

namespace brll
{

class Episode
{
public:
	Episode();
	virtual ~Episode();
	void addExperience(const Experience& e);
	unsigned int getNumExperiences() const ;
	const Experience& getExperience(const unsigned int index) const;

private:
	deque<Experience> experiences;
};

}  // namespace brll

#endif /* EPISODE_H_ */
