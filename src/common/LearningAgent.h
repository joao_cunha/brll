#ifndef LEARNINGAGENT_H_
#define LEARNINGAGENT_H_

#include <iostream>
#include <vector>

#include "Policy.h"
#include "State.h"
#include "Action.h"
#include "Module.h"

using namespace std;

namespace brll
{

class LearningAgent
{
public:
	LearningAgent(Policy* pi);
	virtual ~LearningAgent();
	const Action& decide(const State& s);
	void addModule(Module* m);
	void episodeEnded();

private:
	Policy* pi;
	std::vector<Module*> modules;
};

}  // namespace brll

#endif /* LEARNINGAGENT_H_ */
