#include "Action.h"

#include <cassert>
#include <iostream>

using namespace std;

namespace brll
{

Action::Action()
{

}

Action::Action(double* commands, unsigned int dim)
	: commands(commands, commands + dim)
{
}

Action::Action(const Action& a)
	: commands(a.commands)
{
}

Action::~Action()
{
}

unsigned int Action::getDim() const
{
	return commands.size();
}

double Action::getCommand(unsigned int index) const
{
	assert(index < commands.size());
	return commands[index];
}

bool Action::operator ==(const Action & other) const
{
	for (unsigned int i = 0; i < commands.size(); ++i)
	{
		if(this->getCommand(i) != other.getCommand(i))
			return false;
	}

	return true;
}

bool Action::operator !=(const Action & other) const
{
	return !(*this == other);
}

bool brll::Action::operator <(const Action & other) const
{
	assert(commands.size() == other.commands.size());
	for (unsigned int i = 0; i < commands.size(); ++i)
	{
		if(this->getCommand(i) >= other.getCommand(i))
			return false;
	}

	return true;
}

void Action::write(ostream & os) const
{
	for (unsigned int i = 0; i < commands.size(); ++i)
		os << getCommand(i) << " ";
}

}  // namespace brll

