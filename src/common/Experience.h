#ifndef EXPERIENCE_H_
#define EXPERIENCE_H_

#include "State.h"
#include "Action.h"

namespace brll
{

class Experience
{
public:
	Experience(const State& s,const Action& a);
	Experience(const Experience& e);
	virtual ~Experience();
	const State & getState() const;
	const Action & getAction() const;

private:
	State s;
	Action a;
};

}  // namespace brll

#endif /* EXPERIENCE_H_ */
