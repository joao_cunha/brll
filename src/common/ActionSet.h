#ifndef ACTIONSET_H_
#define ACTIONSET_H_

#include <vector>
#include "Action.h"

using namespace std;

namespace brll
{

class ActionSet
{
public:
	ActionSet();
	virtual ~ActionSet();
	const Action& getAction(unsigned int index) const;
	unsigned int getNumActions() const;
	void addAction(const Action& a);

private:
	vector<Action> actions;
};

}  // namespace brll

#endif /* ACTIONSET_H_ */
