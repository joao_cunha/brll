#ifndef ACTION_H_
#define ACTION_H_

#include <deque>
#include <iostream>

using namespace std;

namespace brll
{

class Action
{
public:
	Action();
	Action(double* commands, unsigned int dim);
	Action(const Action& a);
	virtual ~Action();
	unsigned int getDim() const;
	double getCommand(unsigned int index) const;
	bool operator==(const Action & other) const;
	bool operator!=(const Action & other) const;
	bool operator<(const Action& other) const;
	void write(ostream & os) const;

private:
	deque<double> commands;
};

}  // namespace brll

#endif /* ACTION_H_ */
