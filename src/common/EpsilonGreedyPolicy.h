#ifndef EPSILONGREEDYPOLICY_H_
#define EPSILONGREEDYPOLICY_H_

#include "Policy.h"
#include "ActionSet.h"

namespace brll
{

class EpsilonGreedyPolicy : public Policy
{
public:
	EpsilonGreedyPolicy(const Policy* pi, const ActionSet& actions, const double& exploration);
	virtual ~EpsilonGreedyPolicy();
	virtual const Action& chooseAction(const State& s) const;

private:
	const Policy* pi;
	ActionSet actions;
	const double exploration;

};

}  // namespace brll

#endif /* EPSILONGREEDYPOLICY_H_ */
