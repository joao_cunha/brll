#include "GreedyQPolicy.h"

#include <cmath>
#include <limits>

using namespace std;

namespace brll
{

GreedyQPolicy::GreedyQPolicy(const QFunction* q , const ActionSet& a)
	:q(q), actions(a)
{
}

GreedyQPolicy::~GreedyQPolicy()
{
	delete q;
}

const Action& GreedyQPolicy::chooseAction(const State& s) const
{
	unsigned int bestActionIdx = 0;
	double bestValue = q->getUtility(s, actions.getAction(0));
	for (unsigned int i = 1; i < actions.getNumActions(); ++i)
	{
		const Action& tmpAction = actions.getAction(i);
		double tmpValue = q->getUtility(s, tmpAction);
		if(tmpValue < bestValue)
		{
			bestValue = tmpValue;
			bestActionIdx = i;
		}
	}

	return actions.getAction(bestActionIdx);
}

}  // namespace brll

