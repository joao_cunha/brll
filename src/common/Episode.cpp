#include "Episode.h"
#include <cassert>

namespace brll
{

Episode::Episode()
{
}

Episode::~Episode()
{
}

void Episode::addExperience(const Experience& e)
{
	experiences.push_back(e);
}

unsigned int Episode::getNumExperiences() const
{
	return experiences.size();
}

const Experience& Episode::getExperience(const unsigned int index) const
{
	assert(index < getNumExperiences());
	return experiences.at(index);
}

}  // namespace brll
