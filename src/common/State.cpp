#include "State.h"
#include <cassert>

namespace brll
{

State::State()
{
}

State::State(const double* states, unsigned int dim)
	: states(states, states + dim)
{
}

State::~State()
{
}

double State::getValue(unsigned int dim) const
{
	assert(dim < states.size());
	return states[dim];
}

void State::setValue(unsigned int dim, double value)
{
	assert(dim < states.size());
	states[dim] = value;
}

unsigned int State::getDim() const
{
	return states.size();
}

}  // namespace brll
