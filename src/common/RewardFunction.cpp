#include "RewardFunction.h"

namespace brll
{

RewardFunction::RewardFunction()
{
}

RewardFunction::~RewardFunction()
{
}

double RewardFunction::calcReward(const State& s, const Action& a, const State& sLine) const
{
	double reward;
	if (isStateInSPlus(sLine))
		reward = rewardInSPlus(s,a,sLine);
	else if(isStateInSMinus(sLine))
		reward = rewardInSMinus(s,a,sLine);
	else
		reward = rewardOtherwise(s,a,sLine);

	return reward;
}

}  // namespace brll
