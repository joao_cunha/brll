#ifndef STATE_H_
#define STATE_H_

#include <deque>

namespace brll
{

class State
{
public:
	State();
	State(const double* states, unsigned int dim);
	virtual ~State();
	double getValue(unsigned int dim) const;
	void setValue(unsigned int dim, double value);
	unsigned int getDim() const;

private:
	std::deque<double> states;
};

}  // namespace brll

#endif /* STATE_H_ */
