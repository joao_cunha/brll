#ifndef GREEDYQPOLICY_H_
#define GREEDYQPOLICY_H_

#include "Policy.h"
#include "QFunction.h"
#include "ActionSet.h"

namespace brll
{

class GreedyQPolicy : public Policy
{
public:
	GreedyQPolicy(const QFunction * q, const ActionSet& a);
	virtual ~GreedyQPolicy();
	virtual const Action& chooseAction(const State& s) const;

private:
	const QFunction* q;
	ActionSet actions;
};

}  // namespace brll

#endif /* GREEDYQPOLICY_H_ */
