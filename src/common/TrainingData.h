#ifndef TRAININGDATA_H_
#define TRAININGDATA_H_

#include <deque>
#include <string>
#include "Episode.h"

using namespace std;

namespace brll
{

class TrainingData
{
public:
	TrainingData(const unsigned int stateDim, const unsigned int actionDim);
	TrainingData(const string& filename, const unsigned int stateDim, const unsigned int actionDim,
		const unsigned int maxEpisodes = 0);
	virtual ~TrainingData();
	void addEpisode(const Episode & e);
	unsigned int getNumEpisodes() const;
	const Episode& getEpisode(const unsigned int index) const;
	unsigned int getNumTransitions() const;
	const unsigned int getActionDim() const;
	const unsigned int getStateDim() const;
	void write(const string & filename) const;

private:
	const unsigned int stateDim;
	const unsigned int actionDim;
	deque<Episode> runs;
};

} // namespace brll

#endif /* TRAININGDATA_H_ */
