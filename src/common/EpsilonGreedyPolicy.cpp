#include "EpsilonGreedyPolicy.h"

#include <stdlib.h>

namespace brll
{

EpsilonGreedyPolicy::EpsilonGreedyPolicy(const Policy* pi, const ActionSet& actions, const double& exploration)
	: pi(pi), actions(actions), exploration(exploration)
{
}

EpsilonGreedyPolicy::~EpsilonGreedyPolicy()
{
	delete pi;
}

const Action& EpsilonGreedyPolicy::chooseAction(const State& s) const
{
	const Action& greedyAction = pi->chooseAction(s);

	double rnum = drand48();
	if(rnum < exploration) //will choose an exploring action
	{
		unsigned int rActionIdx = 0;
		do
		{
			rnum = drand48();
			rActionIdx = (unsigned int)(rnum*actions.getNumActions());
			if(rActionIdx == actions.getNumActions()) // this might happen if rnum == 1
				rActionIdx -= 1;
		} while (greedyAction == actions.getAction(rActionIdx));
		return actions.getAction(rActionIdx);
	}
	else
		return greedyAction;
}

}  // namespace brll
