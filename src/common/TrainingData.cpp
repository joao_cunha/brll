#include "TrainingData.h"

#include <cassert>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <iostream>
#include <sstream>

namespace brll
{

TrainingData::TrainingData(const unsigned int stateDim, const unsigned int actionDim)
	: stateDim(stateDim), actionDim(actionDim)
{
}

TrainingData::TrainingData(const string& filename, const unsigned int stateDim,
	const unsigned int actionDim, const unsigned int maxEpisodes) :
	stateDim(stateDim), actionDim(actionDim)
{
	ifstream dataFile;
	dataFile.open(filename.c_str());
	double *s = new double[stateDim];
	double *a = new double[actionDim];
	string line;
	unsigned int numEpisodes = 0;

	while (getline(dataFile, line))
	{
		if (line.compare("---") == 0)
		{
			if ((maxEpisodes > 0) && (numEpisodes > maxEpisodes))
				break;
			runs.push_back(Episode());
			numEpisodes++;
		}
		else
		{
			istringstream ss(line, istringstream::in);
			for (unsigned int i = 0; i < stateDim; ++i)
			{
				double value = -1.0;
				ss >> value;
				s[i] = value;
			}
			State state = State(s, stateDim);
			for (unsigned int i = 0; i < actionDim; ++i)
			{
				double value = -1.0;
				ss >> value;
				a[i] = value;
			}
			Action action = Action(a, actionDim);
			Experience e = Experience(state, action);
			runs.back().addExperience(e);
		}

	}

	delete[] s;
	delete[] a;
}

TrainingData::~TrainingData()
{
//	for (unsigned int i = 0; i < runs.size(); ++i)
//	{
//		delete runs.at(i);
//	}
}

void TrainingData::addEpisode(const Episode & e)
{
	runs.push_back(e);
}

unsigned int TrainingData::getNumEpisodes() const
{
	return runs.size();
}

const Episode& TrainingData::getEpisode(const unsigned int index) const
{
	assert(index < getNumEpisodes());
	return runs.at(index);
}

unsigned int TrainingData::getNumTransitions() const
{
	unsigned int counter = 0;
	for (unsigned int i = 0; i < getNumEpisodes(); ++i)
	{
		counter += (getEpisode(i).getNumExperiences() - 1);
	}

	return counter;
}

const unsigned int TrainingData::getActionDim() const
{
	return actionDim;
}

const unsigned int TrainingData::getStateDim() const
{
	return stateDim;
}

void TrainingData::write(const string& filename) const
{
	ofstream ofs(const_cast<char*>(filename.c_str()), ofstream::trunc);

	for (unsigned int i = 0; i < getNumEpisodes(); ++i)
	{
		const Episode & run = getEpisode(i);

		ofs << "---" << endl;
		for (unsigned int i = 0; i < run.getNumExperiences(); ++i) //for each Experience
		{
			const Experience& e = run.getExperience(i);
			const State& s = e.getState();
			const Action& a = e.getAction();
			for (unsigned int j = 0; j < s.getDim(); ++j)
			{
				ofs << s.getValue(j) << " ";
			}
			for (unsigned int j = 0; j < a.getDim(); ++j)
			{
				ofs << a.getCommand(j) << " ";
			}
			ofs << endl;
		}
	}

	ofs.close();

}

} // namespace brll


