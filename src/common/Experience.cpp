#include "Experience.h"

namespace brll
{

Experience::Experience(const State& s, const Action& a)
	: s(s), a(a)
{
}

Experience::Experience(const Experience& e)
	: s(e.s), a(e.a)
{
}

Experience::~Experience()
{
//	delete s;
//	delete a;
}

const State& Experience::getState() const
{
    return s;
}

const Action& Experience::getAction() const
{
    return a;
}

}  // namespace brll
