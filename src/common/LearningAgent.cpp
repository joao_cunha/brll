#include "LearningAgent.h"

namespace brll
{

LearningAgent::LearningAgent(Policy* pi)
	: pi(pi)
{
}

LearningAgent::~LearningAgent()
{
	delete pi;
}

const Action& LearningAgent::decide(const State& s)
{
	const Action& a = pi->chooseAction(s);
	for (unsigned int i = 0; i < modules.size(); ++i)
		modules.at(i)->update(s,a);
	return a;
}

void LearningAgent::episodeEnded()
{
	for (unsigned int i = 0; i < modules.size(); ++i)
		modules.at(i)->flush();
}

void LearningAgent::addModule(Module* m)
{
	modules.push_back(m);
}

}  // namespace brll
