#ifndef REWARDFUNCTION_H_
#define REWARDFUNCTION_H_

#include <deque>
#include "State.h"
#include "Action.h"

namespace brll
{

class RewardFunction
{
public:
	RewardFunction();
	virtual ~RewardFunction();
	double calcReward(const State& s, const Action& a, const State& sLine) const;
	virtual bool isStateTerminal(const State& s) const = 0;
	virtual bool isStateInSPlus(const State& s) const = 0;
	virtual bool isStateInSMinus(const State& s) const = 0;
	virtual double rewardInSPlus(const State& s, const Action& a, const State& sLine) const = 0;
	virtual double rewardInSMinus(const State& s, const Action& a, const State& sLine) const = 0;
	virtual double rewardOtherwise(const State& s, const Action& a, const State& sLine) const = 0;
};

}  // namespace brll

#endif /* REWARDFUNCTION_H_ */
