#ifndef QFUNCTION_H_
#define QFUNCTION_H_

#include "State.h"
#include "Action.h"
#include "QPattern.h"

namespace brll
{

class QFunction
{

public:
	virtual ~QFunction(){};
	virtual double getUtility(const State& s, const Action& a) const = 0;
	virtual void train(const QPattern& pattern) = 0;
};

}  // namespace brll

#endif /* QFUNCTION_H_ */
