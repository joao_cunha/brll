#ifndef POLICY_H_
#define POLICY_H_

#include "State.h"
#include "Action.h"

namespace brll
{

class Policy
{
public:
	virtual ~Policy() {};
	virtual const Action& chooseAction(const State& s) const = 0;
};

}  // namespace brll

#endif /* POLICY_H_ */
