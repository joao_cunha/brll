#include "ActionSet.h"

#include <cassert>

namespace brll
{

ActionSet::ActionSet()
{
}

ActionSet::~ActionSet()
{
//	for (unsigned int i = 0; i < this->getNumActions(); ++i)
//	{
//		delete getAction(i);
//	}
}

const Action& ActionSet::getAction(unsigned int index) const
{
	assert(index < actions.size());

	return actions[index];
}

void ActionSet::addAction(const Action& a)
{
	actions.push_back(a);
}

unsigned int ActionSet::getNumActions() const
{
	return actions.size();
}

}  // namespace brll
